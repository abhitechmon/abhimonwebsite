import React, { Component } from 'react';
import { Breadcrumb, BreadcrumbItem,
            Button, Form, FormGroup, Label, Input, Col } from 'reactstrap';

export default class Contact extends Component {
    constructor(props) {
        super(props);

        this.state = {
            name: '',
            telnum: '',
            email: '',
            message: ''
        };

        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        
    }

    handleInputChange(event) {
        const target = event.target;
        const value = target.value;    //target.type === 'checkbox' ? target.checked :
        const name = target.name;
    
        this.setState({
          [name]: value
        });
    }

    handleSubmit(event) {
        console.log('Current State is: ' + JSON.stringify(this.state));
        alert('Current State is: ' + JSON.stringify(this.state));
        event.preventDefault();
    }

    render() {
        return(
            <div className="container">
                <h2 className="whiteColor">Contact</h2>
                <div className="row row-content contactRow">
                   <div className="col-12">
                      <h4 className="redColor">Have a question or want to work together?</h4>
                      <br/>
                   </div>
                    <div className="col-12 col-md-8">
                        <Form onSubmit={this.handleSubmit}>
                            <FormGroup row>
                                <Label htmlFor="name" md={2} className="whiteColor">Name</Label>
                                <Col md={10}>
                                    <Input type="text" id="name" name="name"
                                        placeholder="Name"
                                        value={this.state.firstname}
                                        onChange={this.handleInputChange} />
                                </Col>
                            </FormGroup>
                            
                            <FormGroup row>
                            <Label htmlFor="telnum" md={2} className="whiteColor">Contact</Label>
                                <Col md={10}>
                                    <Input type="tel" id="telnum" name="telnum"
                                        placeholder="Tel. number"
                                        value={this.state.telnum}
                                        onChange={this.handleInputChange} />
                                </Col>
                            </FormGroup>
                            <FormGroup row>
                                <Label htmlFor="email" md={2} className="whiteColor">Email</Label>
                                <Col md={10}>
                                    <Input type="email" id="email" name="email"
                                        placeholder="Email"
                                        value={this.state.email}
                                        onChange={this.handleInputChange} />
                                </Col>
                            </FormGroup>
                        
                            <FormGroup row>
                                <Label htmlFor="message" md={2} className="whiteColor message">Your Message</Label>
                                <Col md={10}>
                                    <Input type="textarea" id="message" name="message"
                                        rows="12"
                                        value={this.state.message}
                                        onChange={this.handleInputChange}></Input>
                                </Col>
                            </FormGroup>
                            <FormGroup row>
                                <Col md={{size: 10, offset: 2}}>
                                    <Button type="submit" color="danger">
                                        Send Feedback
                                    </Button>
                                </Col>
                            </FormGroup>
                        </Form>
                    </div>
                    <div className="col-12 justify-contents-center">
                        <iframe title="model" width="800" height="500" frameBorder="0" src="https://scapic.com/@Abhimon/ar_business_card-6oqs1/embed/?type=viewer" />
                    </div>
               </div>
               
            </div>                     
        );          
    }
}