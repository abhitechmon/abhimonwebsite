import React from 'react';
import {
  Card, CardImg, CardText, CardBody,
  CardTitle, CardSubtitle, Button
} from 'reactstrap';

function Home(props) {
    return(
    <React.Fragment>
      <div className="container">
        {/* <h2 className="whiteColor">Home</h2> */}
        
        <div className="row">
          <div className="col-sm-6">
            <div className="video-container">
              <iframe width="560" height="315" src="https://www.youtube.com/embed/-WR-FyUQc6I" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
          </div>
          <div className="col-sm-6">
            <p><i class="fa fa-quote-left redColor"></i> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam bibendum in leo vitae luctus. Ut sapien est, hendrerit quis urna vel, lobortis mollis metus. Fusce rutrum, elit a interdum iaculis, ipsum ex hendrerit tellus, a iaculis sem odio in lorem. In ac mauris in purus mollis venenatis. Sed euismod lectus leo, at porta nunc iaculis quis.</p>
          </div>
        </div>
      </div>
        

      <div className="container">
        <h2 className="whiteColor">About</h2>
      
        <div className="row">
          <div className="col-sm-3">
            <div className="hex-wrap">
              <div className="hexagon">
                <i class="fa fa-fast-forward hexaicon"></i>
              </div>
            </div>
            <div>
              Fast
            </div>
            <p>Fast load times and lag free interaction</p>
          </div>
          <div className="col-sm-3">
            <div className="hex-wrap">
              <div className="hexagon">
                <i class="fa fa-fast-forward hexaicon"></i>
              </div>
            </div>
            <div>
              Fast
            </div>
            <p>Fast load times and lag free interaction</p>
          </div>
          <div className="col-sm-3">
            <div className="hex-wrap">
              <div className="hexagon">
                <i class="fa fa-fast-forward hexaicon"></i>
              </div>
            </div>
            <div>
              Fast
            </div>
            <p>Fast load times and lag free interaction</p>
          </div>
          <div className="col-sm-3">
            <div className="hex-wrap">
              <div className="hexagon">
                <i class="fa fa-fast-forward hexaicon"></i>
              </div>
            </div>
            <div>
              Fast
            </div>
            <p>Fast load times and lag free interaction</p>
          </div>
        </div>
        
      </div>

      <div className="container">
        <div className="row justify-content-center">
        <iframe title="model" width="800" height="500" frameBorder="5" src="https://scapic.com/@Abhimon/ar_business_card-6oqs1/embed/?type=viewer" />    
        </div>
      </div>
    
    </React.Fragment>  
      
     
    );
}

export default Home;   