import React, {Component} from 'react';
import Home from './HomeComponent';
import Contact from './ContactComponent';
import Resume from './ResumeComponent';
import Header from './HeaderComponent';
import Footer from './FooterComponent';
import { Switch, Route, Redirect } from 'react-router-dom';

class Main extends Component{
    render(){
        return(
            <div>
                <Header />
                <Switch>
                    <Route path='/home'  component={Home}/>
                    <Route path='/contact'  component={Contact}/>
                    <Route path="/resume" component={Resume}/>
                    <Redirect to="/home" />
                </Switch>
                
                <Footer />
                
            </div>
        );
    }
}

export default Main;